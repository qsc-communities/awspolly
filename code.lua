-------------------
--[[

Copyright 2021 QSC, LLC

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this softwareand associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

]]--

--[[

AWS Polly Text-To-Speech Example

This script uses Amazon Polly ( https://docs.aws.amazon.com/polly/index.html ) to convert
text into an audio file and then play the resultant audio file out a Q-SYS Audio Player

It requires an AWS account and an AWS IAM user
with full AmazonPollyFullAccess enabled

]]--

json = require('rapidjson')
fileName = "test.mp3"  
algorithm  = "AWS4-HMAC-SHA256"
hash = "sha256"

requestMethod = "POST"
host = "polly.us-east-1.amazonaws.com" -- this may change depending on the location of your AWS account
service = "polly"
uri = "/v1/speech"

queryArgs = "" -- no query args required
contentType = "application/json"

url = string.format("https://%s%s", host, uri)

-- these are voices that support the neural network engine
neuralVoices = {
  "Amy", "Brian", "Camila", "Emma", "Ivy", "Joanna", "Joey", "Justin", 
  "Kendra", "Kevin", "Kimberly",  "Lupe", "Matthew", "Olivia",  "Salli", "Seoyeon"
}

-- these voices support the standard engine
standardVoices = {
  "Aditi", "Astrid", "Bianca", "Carla", "Carmen", "Celine", "Chantal", "Conchita",
  "Cristiano", "Dora", "Enrique", "Ewa", "Filiz", "Geraint", "Giorgio", "Gwyneth",
  "Hans", "Ines", "Jacek", "Jan", "Karl", "Lea", "Liv", "Lotte", "Lucia", "Mads",
  "Maja", "Marlene", "Mathieu", "Maxim", "Mia", "Miguel", "Mizuki", "Naja", "Nicole",
  "Penelope", "Raveena", "Ricardo", "Ruben", "Russell", "Takumi", "Tatyana", "Vicki", 
  "Vitoria", "Zeina", "Zhiyu"
}
-- initialize voices
voiceChoices = {}
for k,v in pairs(neuralVoices) do table.insert(voiceChoices, v) end
for k,v in pairs(standardVoices) do table.insert(voiceChoices, v) end
table.sort(voiceChoices)

Controls.voice.Choices = voiceChoices
if #Controls.voice.String == 0 then Controls.voice.String = "Joanna" end

-- initialize languages
Controls.language.Choices = {
  "arb", "cmn-CN", "cy-GB", "da-DK", "de-DE", "en-AU", "en-GB", "en-GB-WLS",
  "en-IN", "en-US", "es-ES", "es-MX", "es-US", "fr-CA", "fr-FR", "is-IS",
  "it-IT", "ja-JP", "hi-IN", "ko-KR", "nb-NO", "nl-NL", "pl-PL", "pt-BR",
  "pt-PT", "ro-RO", "ru-RU", "sv-SE", "tr-TR"
}
if #Controls.language.String == 0 then Controls.language.String = "en-US" end

Controls.fileName.String = fileName

-- helper functions

-- convert bytes to hex encoded lowercase string
function hexEncode(str)
  return (str:gsub('.', function (c)
    return string.format('%02x', string.byte(c))
  end))
end

-- calculate hash of string, hex encode the result and return
function hashString(str)
  return hexEncode(Crypto.Digest(hash, str))
end

-- return a sorted list of the keys for the table
-- with an optional transform function
function getTableKeys(tbl, tx)
  local keys = {}
  for k,v in pairs(tbl) do 
    if tx then k = tx(k) end
    table.insert(keys, k)
  end
  table.sort(keys)
  return keys
end

-- synthesize text and play 
function synthesizeVoice(voice, text, lang)
  local accessId = Controls.accessId.String
  local accessKey = Controls.accessKey.String
  
  if #accessId == 0 then
    Controls.status.Value = 2
    Controls.status.String = "access id required"
    return
  end
  
  if #accessId == 0 then
    Controls.status.Value = 2
    Controls.status.String = "access key required"
    return
  end

  local requestDateTime = os.date("!%Y%m%dT%H%M%SZ")
  local requestDate = os.date("!%Y%m%d")
  local credentialScope = requestDate.."/us-east-1/"..service.."/aws4_request"
  
  local headers = {
    ["Content-Type"] = contentType,
    host = host,
    ["x-amz-date"] = requestDateTime,
  }
  -- list of headers needs to be lowercase and alphabetical
  local lowerCaseHeaders = {} 
  for k,v in pairs(headers) do
    table.insert(lowerCaseHeaders, string.lower(k))
  end
  table.sort(lowerCaseHeaders)
  
  local signedHeaders = table.concat(lowerCaseHeaders, ";")
  
  -- determine which engine to use
  local engine = "standard"
  for k,v in pairs(neuralVoices) do
    if v == voice then engine = "neural" end
  end
  
  -- https://docs.aws.amazon.com/polly/latest/dg/API_SynthesizeSpeech.html
  local payload = {
    Engine = engine,
    LanguageCode = lang,
    OutputFormat = "mp3",
    Text = "<speak>"..text.."</speak>",
    TextType = "ssml",
    VoiceId = voice
  }
  
  -- 
  -- Follows steps from https://docs.aws.amazon.com/general/latest/gr/sigv4_signing.html
  -- to correctly sign our HTTP request to AWS
  
  -- JSON formatted request sent to Polly
  local payloadString = json.encode(payload)
  
  --
  -- Create a canonical request
  -- https://docs.aws.amazon.com/general/latest/gr/sigv4-create-canonical-request.html
  --
  
  -- canonical request is in the form
  -- METHOD\n
  -- URI\n
  -- QUERYARGS\n
  -- HEADERS\n ( lower case and in alphabetical order )
  -- \n
  -- SIGNEDHEADERS\n
  -- HASHEDPAYLOAD
  
  local canonicalRequestTable = {}
  table.insert(canonicalRequestTable, requestMethod)
  table.insert(canonicalRequestTable, uri)
  table.insert(canonicalRequestTable, queryArgs)
  
  -- the headers need to go in alphabetical order and lowercase
  -- first create a new table with lowercase keys with a companion
  -- list of keys. Then sort the keys and iterate them to add our 
  -- headers to the canonical request
  local lowerCaseHeaders = {} -- table of lower case keys to values
  local lowerCaseHeaderKeys = {} -- array of lower case keys we can sort
  for k,v in pairs(headers) do 
    lowerCaseHeaders[string.lower(k)] = v 
    table.insert(lowerCaseHeaderKeys, string.lower(k))
  end
  table.sort(lowerCaseHeaderKeys)
  
  for k,v in pairs(lowerCaseHeaderKeys) do
    table.insert(canonicalRequestTable, v..":"..lowerCaseHeaders[v])
  end
  table.insert(canonicalRequestTable, "")  -- blank line
  table.insert(canonicalRequestTable, signedHeaders)
  table.insert(canonicalRequestTable, hashString(payloadString))
  
  canonicalRequest = table.concat(canonicalRequestTable, "\n")
  local hashedCanonicalRequest = hashString(canonicalRequest)
  --
  -- Create a string to sign
  -- https://docs.aws.amazon.com/general/latest/gr/sigv4-create-string-to-sign.html
  --
  local stringToSign = string.format("%s\n%s\n%s\n%s", algorithm, requestDateTime, credentialScope, hashedCanonicalRequest)
  --
  -- Calculate signature 
  -- https://docs.aws.amazon.com/general/latest/gr/sigv4-calculate-signature.html
  --
  local kDate = Crypto.HMAC(hash, "AWS4"..accessKey, requestDate)
  local kRegion = Crypto.HMAC(hash, kDate, "us-east-1") -- this will change if the region changes
  local kService = Crypto.HMAC(hash, kRegion, service)
  local kSigning = Crypto.HMAC(hash, kService, "aws4_request")
  local signature = hexEncode(Crypto.HMAC(hash, kSigning, stringToSign))
  
  --
  -- Add the signature to the HTTP request
  -- https://docs.aws.amazon.com/general/latest/gr/sigv4-add-signature-to-request.html
  --
  -- Authorization: algorithm Credential=access key ID/credential scope, SignedHeaders=SignedHeaders, Signature=signature
  headers.Authorization = string.format("%s Credential=%s, SignedHeaders=%s, Signature=%s", algorithm, accessId.."/"..credentialScope, signedHeaders, signature )
  
  --
  -- Make HTTP request
  --
  HttpClient.Upload 
  {
    Url = url,
    Method = requestMethod,
    Headers = headers,
    Data = payloadString,
    EventHandler = function(_,code,data,err) 
      if code == 200 then
        local file, err = io.open("media/Audio/"..fileName, "wb")
        file:write(data)
        file:close()
        
        -- since we are overwriting a file that the audio file player
        -- is currently pointed at we delay the start of the playback
        -- so it has a chance to load the new file. 
        Timer.CallAfter(function() Controls.play:Trigger() end, 0.250 )
        Controls.status.Value = 0
      else
        Controls.status.Value = 2
        Controls.status.String = data
        print(string.format("ERROR %i : %s", code, data))
      end
    end,
  }
end

Controls.go.EventHandler = function()
 synthesizeVoice(Controls.voice.String, Controls.textToSay.String, Controls.language.String)
end

