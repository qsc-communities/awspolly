# Q-SYS AWS Polly Text to Speech Example #

This example script interfaces to the [Amazon AWS Polly](https://aws.amazon.com/polly/) service to convert text data into speech ( aka an audio file ) and playback the result. An AWS account is required and an IAM account needs to be enabled with AmazonPollyFullAccess enabled.

### AWS & Polly ###

Amazon Web Services ( AWS ) is a cloud service availble to both businesses and individuals. AWS provides numerous services - this example leverages Polly for its Text to Speech ( TTS ) capabilities. The free tier of AWS does support Polly for 12 months with limitations on the number of characters processed ( 5M for standard TTS and 1M for neural TTS ). 

Because of both the time and character limitations it would be unwise to deploy a system using the free tier. **QSC is not responsible for any AWS charges that may be incurred!** Please make sure you and your customers understand any cost implications that may be incurred by using Polly or any other AWS service in a production environment.

### AWS account ###

Create a free tier AWS account and follow [these](https://docs.aws.amazon.com/polly/latest/dg/setting-up.html) steps to create the Polly access account.

![step 1](images/account1.png)
![step 2](images/account2.png)
![step 3](images/account3.png)
![step 4](images/account4.png)
![step 5](images/account5.png)

After creating the account you will need the Access Key ID and Secrert access key.

### Q-SYS Design ###

The example is currently implmented as a Text Controller which is then wired to an Audio Player.

![qsd](images/qsd.png)

The Text controller needs to be configured with the following controls

* accessId -Text | Text Box
* accessKey - Text | Text Box
* fileName - Indicator | Text | Output Control Pin
* go - Button | Trigger
* language - Text | Combo Box
* play - Button | Trigger | Output Control Pin
* status - Indicator | Status
* textToSay - Text | Text Box
* voice | Text | Combo Box
  
The fileName and play controls need to be wired to the Audio Player as shown in the image.

### Usage ###

To use Polly first paste in the Access ID and Access Key that was copied from the AWS Management Console. Then select a voice and a language, type some text into the textToSay control and hit go. The API calls need to be accurately timestamped - if you Core clock isn't synchronized correctly you may get the following error

    ERROR 403 : {"message":"Signature not yet current: 20210512T183738Z is still later than 20210512T165331Z (20210512T164831Z + 5 min.)"}

Some of the voices are neural network based - these tend to sound quite a bit better than the standard voices but are also have more limited 'free plays'. This [table](https://docs.aws.amazon.com/polly/latest/dg/voicelist.html) shows which voices are neural enabled.
